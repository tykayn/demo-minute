import { TestBed } from '@angular/core/testing';

import { GameDetteService } from './game-dette.service';

describe('GameDetteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GameDetteService = TestBed.get(GameDetteService);
    expect(service).toBeTruthy();
  });
});
