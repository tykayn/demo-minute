import {Injectable} from '@angular/core';
import {Socket} from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  public messages = [];

  constructor(private socket: Socket) {
  }

  sendMessage(msg: string) {
    this.socket.emit('message', msg);
  }

  getMessage() {
    return this.socket
      .fromEvent('message')
      .subscribe((data: any) => {
        this.messages.push(data.msg);
      });
  }
}
