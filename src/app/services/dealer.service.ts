import {EventEmitter, Injectable, Output} from '@angular/core';
import {Card} from '../models/card';
import {Deck} from '../models/deck';
import {Player} from '../models/player';

@Injectable({
  providedIn: 'root'
})
export class DealerService {

  private deck: Deck;
  public players: Player[] = [];
  public loadedGame: any;
  public enableRedistribution = true;
  public table: Card[] = []; // the place where cards fight
  private graveyard: Card[] = [];
  public deckRed: Deck;
  public deckBlue: Deck;
  public gameOptions: any = {
    delayAnimation: false, // wait a little between turns to show animation
    delayAnimationMilliseconds: 100, // wait a little between turns to show animation
    maxTurns: 100, // wait a little between turns to show animation
    redCards: 0,
    blueCards: 6,
    players: 4,
    smallNumberWins: true,
  };
  private fightTurn = 0;
  private turn = 0;
  public logs = [];
  public logPlayerTable = [];
  public dataCanevasStackedScores = [];
  @Output() emitter: EventEmitter<any> = new EventEmitter();
  private graphTurn = 0;

  constructor() {
  }


  createPlayersFromOptions() {
    this.players = [];
    for (let ii = 1; ii <= this.gameOptions.players; ii++) {
      this.players.push(new Player('joueur ' + ii));
    }
  }

  /**
   * débuter la partie de monnaie libre
   */
  startLibre() {

    this.createPlayersFromOptions();

    this.turn = 0;
    this.logPlayerTable = [];
    this.log('initiate players done');
    this.log('give players 3 cards');
    this.startBatailleSetup();
    this.log('run fights');
    this.runTurns(this.gameOptions.maxTurns);
  }

  /**
   * log info
   */
  log(text: string) {
    // tslint:disable-next-line:no-console
    console.info('=== ', text);
    this.logs.push(text);
  }

  runTurns(limit) {
    if (limit > this.gameOptions.maxTurns) {
      limit = this.gameOptions.maxTurns;
    }
    if (this.gameOptions.delayAnimation) {
      // tslint:disable-next-line:no-shadowed-variable
      for (let ii = 0; ii < limit; ii++) {
        setTimeout(
          () => {
            this.runFightTurn();

          }, this.gameOptions.delayAnimationMilliseconds
        );
      }
    } else {
      for (let ii = 0; ii < limit; ii++) {
        this.runFightTurn();
      }
    }

    this.emitEndOfTurns();
    this.log(' end of turns ');
  }

  emitEndOfTurns() {
    console.log('event emitEndOfTurns');
    this.emitter.emit(this.dataCanevasStackedScores);
  }

  runFightTurn() {
    this.log('-------------------------');
    this.log('next bataille turn begins');
    this.turn++;
    this.putCardOnTableForAllPlayers();
    this.fightBataille(this.table);
    this.logPlayersScore();
    if (this.enableRedistribution) {

      this.redistribute();
    }
    this.logPlayersScore('with redistribution');
  }


  redistribute() {
    const average = 6;
    let playerGiving;
    let playerRecieving;
    // find the players having more than average of blue cards
    this.players.map(player => {
      if (player.handBlue.length > average) {
        playerGiving = player;
      } else if (player.handBlue.length < average) {
        playerRecieving = player;
      }
    });
    if (playerRecieving && playerGiving && playerGiving.handBlue.length) {

      const cardGiven = playerGiving.giveRandomCard('handBlue');
      playerRecieving.recieveCard(cardGiven);
    }
  }

  /**
   * make a fight between cards on the table
   */
  fightBataille(cards: Card[] = this.table) {

    this.log('   fight of the cards, turn ' + this.turn);
    if (this.gameOptions.smallNumberWins) {
      this.log('   the smallest value wins ' + this.gameOptions.smallNumberWins);
    }
    // find greater value between cards
    let max = 0;
    let min = 13;
    let countExAequo = 0;

    cards.map(card => {
      if (card.valueInt > max) {
        max = card.valueInt;
      }
      if (card.valueInt < min) {
        min = card.valueInt;
      }
    });

    const exaequoCards = [];
    let lastMaxCard = null;
    let lastMinCard = null;

    cards.map(card => {
      if (this.gameOptions.smallNumberWins) {
        if (card.valueInt === min) {
          countExAequo++;
          lastMinCard = card;
          exaequoCards.push(card);
        }
      } else {
        if (card.valueInt === max) {
          countExAequo++;
          lastMaxCard = card;
          exaequoCards.push(card);
        }
      }

    });

    if (countExAequo > 1) {
      this.log(' there is ex aequo');
      this.fightTurn++;
      // this.fightBataille(exaequoCards);
      // for the exaequo cards, fight again.
      // const otherCardsToFight = this.playersGiveCards(1);
      // console.log(' ********** ex aequo', exaequoCards);
      this.giveBackToPlayers(cards);

    } else {

      if (this.gameOptions.smallNumberWins) {
        this.log(' ' + lastMinCard.owner.name + ' wins and gets all the cards on the table');
        lastMinCard.owner.victories++;
        cards.map(card => {
          card.owner = lastMinCard.owner;
          lastMinCard.owner.handBlue.push(card);
        });

        this.log(' ' + lastMinCard.owner.name + ' now has ' + lastMinCard.owner.handBlue.length + ' cards');
        this.table = [];

        return lastMinCard.owner;
      } else {
        this.log(' ' + lastMaxCard.owner.name + ' wins and gets all the cards on the table');
        lastMaxCard.owner.victories++;
        cards.map(card => {
          card.owner = lastMaxCard.owner;
          lastMaxCard.owner.handBlue.push(card);
        });

        this.log(' ' + lastMaxCard.owner.name + ' now has ' + lastMaxCard.owner.handBlue.length + ' cards');
        this.table = [];

        return lastMaxCard.owner;
      }

    }
  }

  giveBackToPlayers(cards: Card[]) {
    cards.map(card => {
      card.owner.handBlue.push(card);
    });
    this.table = [];
  }

  startBatailleSetup() {
    this.turn = 1;
    // utiliser deux paquets de 52 cartes
    this.deckBlue = new Deck(52, {
      type: 'classic',
      color: 'blue',
    });
    this.deckRed = new Deck(52, {
      type: 'classic',
      color: 'red',
    });
    // shuffle cards
    this.deckBlue.shuffle();
    this.deckRed.shuffle();

    this.initialGiveCardsToPlayers();
  }

  /**
   * all players are giving one card randomly
   */
  putCardOnTableForAllPlayers() {
    this.log('   each player puts one card on the table');
    this.players.map(player => {
      if (!player.handBlue.length) {
        this.log('player ' + player.name + ' has no more cards, damn!');
      } else {

        this.table.push(player.giveRandomCard('handBlue'));
      }
    });
  }

  /**
   * distribute 3 red cards and 3 blue cards to players
   */
  initialGiveCardsToPlayers() {

    const numberOfCards = this.gameOptions.blueCards;

    if (this.gameOptions.blueCards) {
      this.players.map((player) => {
        this.giveSomeCardsToPlayer(this.deckBlue, this.gameOptions.blueCards, player);
      });
    }
    if (this.gameOptions.redCards) {
      this.players.map((player) => {
        this.giveSomeCardsToPlayer(this.deckRed, this.gameOptions.redCards, player);
      });
    }
  }

  /**
   * pick randomly a few cards from a deck and give them to the player's hand
   */
  giveSomeCardsToPlayer(deck: Deck, cardsCount: number, player: Player) {
    for (let ii = 0; ii <= cardsCount; ii++) {
      player.recieveCard(deck.cards.pop());
    }
  }


  startDebt() {

  }

  private logPlayersScore(message = '') {
    const playerTable = [];
    playerTable.push(this.turn);
    playerTable.push(message);
    let sum = 0;
    const tempScores = [];
    this.players.map(player => {
      sum += player.handBlue.length;
      tempScores.push([player.handBlue.length]);
    });
    playerTable.push(sum);
    playerTable.push(...tempScores);
    this.logPlayerTable.push(playerTable);
    this.addChartStackedData();
  }

  private addChartStackedData() {
    const newDataPointsForThisTurn = this.players.map((player) => {
      return {y: player.handBlue.length, label: player.name, x: this.graphTurn};
    });
    this.dataCanevasStackedScores.push({
      type: 'stackedArea100',
      name: 'tour ' + this.turn,
      showInLegend: 'true',
      dataPoints: newDataPointsForThisTurn,
    });
    this.graphTurn++;
  }
}
