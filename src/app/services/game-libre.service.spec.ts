import { TestBed } from '@angular/core/testing';

import { GameLibreService } from './game-libre.service';

describe('GameLibreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GameLibreService = TestBed.get(GameLibreService);
    expect(service).toBeTruthy();
  });
});
