import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MonnaieLibreComponent} from './page/monnaie-libre/monnaie-libre.component';
import {MonnaieDetteComponent} from './page/dette/monnaie-dette.component';


const routes: Routes = [
  {path: 'bataille/monnaie-dette', component: MonnaieDetteComponent},
  {path: 'bataille/monnaie-libre', component: MonnaieLibreComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
