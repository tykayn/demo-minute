import {Component, Input, OnInit} from '@angular/core';
import {Player} from '../../models/player';
import {DealerService} from '../../services/dealer.service';

@Component({
  selector: 'app-table-scores',
  templateUrl: './table-scores.component.html',
  styleUrls: ['./table-scores.component.scss']
})
export class TableScoresComponent implements OnInit {

  @Input() players: Player[];
  @Input() logPlayerTable: any;

  constructor(public dealer: DealerService) {
  }

  ngOnInit() {
  }

}
