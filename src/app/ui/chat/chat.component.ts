import {Component, OnInit} from '@angular/core';
import {ChatService} from '../../services/chat.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {

  public messages = [];
  newMessage: any;

  constructor(private chat: ChatService) {

    this.messages = chat.messages;
  }

  sendMessageAndClearInput(message) {
    this.chat.sendMessage(message);
    this.newMessage = '';
  }

  ngOnInit() {
  }

}
