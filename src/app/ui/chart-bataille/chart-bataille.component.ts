import {Component, OnInit} from '@angular/core';
import {DealerService} from '../../services/dealer.service';
import * as Chart from 'chartjs/chart';

@Component({
  selector: 'app-chart-bataille',
  templateUrl: './chart-bataille.component.html',
  styleUrls: ['./chart-bataille.component.scss']
})
export class ChartBatailleComponent implements OnInit {

  chart: any;

  constructor(public dealer: DealerService) {
  }


  ngOnInit() {
    // this.dealer.emitter.subscribe(
    //   event => {
    //     console.log('event', event);
    //     this.renderStackedScoresByTurn();
    //   }
    // );
    this.exampleChart();
  }

  exampleChart() {
    // let ctx = document.getElementById('myChart').getContext('2d');
    // let myChart = new Chart(ctx, {
    //   type: 'bar',
    //   data: {
    //     labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
    //     datasets: [{
    //       label: '# of Votes',
    //       data: [12, 19, 3, 5, 2, 3],
    //       backgroundColor: [
    //         'rgba(255, 99, 132, 0.2)',
    //         'rgba(54, 162, 235, 0.2)',
    //         'rgba(255, 206, 86, 0.2)',
    //         'rgba(75, 192, 192, 0.2)',
    //         'rgba(153, 102, 255, 0.2)',
    //         'rgba(255, 159, 64, 0.2)'
    //       ],
    //       borderColor: [
    //         'rgba(255, 99, 132, 1)',
    //         'rgba(54, 162, 235, 1)',
    //         'rgba(255, 206, 86, 1)',
    //         'rgba(75, 192, 192, 1)',
    //         'rgba(153, 102, 255, 1)',
    //         'rgba(255, 159, 64, 1)'
    //       ],
    //       borderWidth: 1
    //     }]
    //   },
    //   options: {
    //     scales: {
    //       yAxes: [{
    //         ticks: {
    //           beginAtZero: true
    //         }
    //       }]
    //     }
    //   }
    // });
  }

  private renderStackedScoresByTurn() {

    // this.chart = new CanvasJS.Chart('chartContainer', {
    //   animationEnabled: true,
    //   title: {
    //     text: 'Cartes par joueur à chaque tour'
    //   },
    //   axisX: {
    //     title: 'Tours de partie',
    //     minimum: 0,
    //     maximum: this.dealer.gameOptions.blueCards * this.dealer.gameOptions.players
    //   },
    //   axisY: {
    //     title: 'Cartes'
    //   },
    //   toolTip: {
    //     shared: true
    //   },
    //   // data: this.dealer.dataCanevasStackedScores,
    //   data: [
    //     {
    //       type: 'stackedArea100',
    //       name: 'tour ' + 1,
    //       showInLegend: 'true',
    //       dataPoints: [
    //         {y: 5, label: 'haaaa', x: 1},
    //         {y: 5, label: 'haaaa 2', x: 1},
    //         {y: 5, label: 'haaaa 3', x: 1},
    //
    //       ],
    //     }
    //   ],
    // });
    // console.log(' this.dealer.dataCanevasStackedScores', this.dealer.dataCanevasStackedScores);
    // this.chart.render();
  }
}
