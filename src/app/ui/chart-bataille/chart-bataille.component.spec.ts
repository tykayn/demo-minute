import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartBatailleComponent } from './chart-bataille.component';

describe('ChartBatailleComponent', () => {
  let component: ChartBatailleComponent;
  let fixture: ComponentFixture<ChartBatailleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartBatailleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartBatailleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
