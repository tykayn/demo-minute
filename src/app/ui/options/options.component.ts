import {Component, OnInit} from '@angular/core';
import {DealerService} from '../../services/dealer.service';

@Component({
  selector: 'app-options',
  templateUrl: './options.component.html',
  styleUrls: ['./options.component.scss']
})
export class OptionsComponent implements OnInit {

  constructor(public dealer: DealerService) {
  }

  ngOnInit() {
  }

}
