import {Card} from './card';

export class Player {

  public hand: Card[] = [];
  public handBlue: Card[] = [];
  public handRed: Card[] = [];
  private id: number;
  public name: string;
  private score: number;
  public victories = 0;
  private debt: number;

  constructor(name: string = 'a player has no name') {
    this.name = name;
  }

  describe() {
    return `${this.name} dispose de ${this.hand ? this.hand.length : 'aucune'} cartes, a un score de ${this.score} points`;
  }

  public recieveCard(card: Card) {
    card.owner = this;
    if (card.color === 'blue') {
      this.handBlue.push(card);
    } else if (card.color === 'red') {
      this.handRed.push(card);
    } else {
      this.hand.push(card);
    }
  }

  public shuffleDeck(deckId) {
    this[deckId] = this.shuffle(this[deckId]);
  }

  public giveRandomCard(deckId) {
    this.shuffleDeck(deckId);
    return this[deckId].pop();
  }

  /**
   * shuffle the cards of this deck
   */
  public shuffle(deck) {

    const arra1 = deck;
    let ctr = arra1.length;
    let temp;
    let index;

    // While there are elements in the array
    while (ctr > 0) {
      // Pick a random index
      index = Math.floor(Math.random() * ctr);
      // Decrease ctr by 1
      ctr--;
      // And swap the last element with it
      temp = arra1[ctr];
      arra1[ctr] = arra1[index];
      arra1[index] = temp;
    }
    return arra1;

  }
}
