import {Player} from './player';

interface CardOptions {
  color: string;
  family: string;
  value: string;
  valueText: string;
  valueInt: number;
}

const defaultCardoptions = {
  color: 'not provided',
  family: 'not provided',
  value: '0',
  valueText: 'not provided',
  valueInt: 0,
};

export class Card {
  public family: string;
  public description: string;
  public valueText: string;
  public valueInt: number;
  public value: string;
  public color: string;
  public isHigh = false;
  public isVisible = false;
  public isInGraveyard = false;
  public owner: Player | null;

  constructor(options: CardOptions = defaultCardoptions) {
    this.color = options.color;
    this.family = options.family;
    this.value = options.value;
    this.valueInt = options.valueInt;
    this.valueText = options.valueText;
  }
}
