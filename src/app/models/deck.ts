import {Card} from './card';

interface DeckOptions {
  addJokers?: boolean;
  color: string;
  type: string;
}

const cardFamilies = {
  fr: ['carreau', 'coeur', 'trèfle', 'pique'],
  en: ['diamond', 'heart', 'clover', 'spade'],
};


export class Deck {
  public cards: Card[] = [];
  public color: string;
  private classicCardColors = ['red', 'black'];
  private cardNameFromValue = ['Ace',
    'Two',
    'Three',
    'Four',
    'Five',
    'Six',
    'Seven',
    'Eight',
    'Nine',
    'Valet',
    'Queen',
    'King'];
  private classicCardValues = ['A', '2', '3', '4', '5', '6', '7', '8', '9', 'V', 'Q', 'K'];
  private classicCardValuesInt = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
  private classicCardFamilies = cardFamilies.fr;
  private hasDistributed = false;

  constructor(countCards = 52, option: DeckOptions = {type: 'classic', color: 'blue'}) {
    if (option.type === 'classic') {
      this.generateClassicDeck(option);
    }
  }

  private generateClassicDeck(option: DeckOptions) {
    if (option.color) {
      this.classicCardFamilies.map((family, value) => {
        this.classicCardValues.map((CardValue, ii) => {
          this.cards.push(
            new Card({
              color: option.color,
              family,
              valueText: this.cardNameFromValue[ii],
              value: this.classicCardValues[ii],
              valueInt: this.classicCardValuesInt[ii],
            })
          );
        });
      });
    }
    if (option.addJokers) {
      const jokers = [
        this.generateJoker(option.color),
        this.generateJoker(option.color),
        this.generateJoker(option.color),
        this.generateJoker(option.color),
      ];
      this.cards.concat(jokers);
    }
  }

  /**
   * make a joker card
   * @param color
   */
  private generateJoker(color: string = 'blue') {
    return new Card({
      color,
      family: 'joker',
      value: 'joker',
      valueText: 'joker',
      valueInt: Infinity,
    });
  }

  /**
   * shuffle the cards of this deck
   */
  public shuffle() {

    const arra1 = this.cards;
    let ctr = arra1.length;
    let temp;
    let index;

    // While there are elements in the array
    while (ctr > 0) {
      // Pick a random index
      index = Math.floor(Math.random() * ctr);
      // Decrease ctr by 1
      ctr--;
      // And swap the last element with it
      temp = arra1[ctr];
      arra1[ctr] = arra1[index];
      arra1[index] = temp;
    }
    return arra1;

  }

}
