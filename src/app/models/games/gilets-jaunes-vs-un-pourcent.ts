import {Game} from './game';
import {Deck} from '../deck';
import {Card} from '../card';

export class GiletsJaunesVsUnPourcent extends Game {
  name = 'Gilets jaune vs un pourcent';
  gameLengthInMinutes = 10;
  playersCount = {
    min: 2,
    max: 5
  };
  playersYearsOld = {
    min: 8,
    max: 999
  };
  licence = 'CC-nc-by-sa';
  url = 'https://forum.monnaie-libre.fr/t/prototype-un-pourcent-contre-gilets-jaunes/8778/2';
  gameElements = {
    roulette: [],
    credits: [],
    ressource: [],
  };
  private turn = 0;

  init() {
    this.initCards();
  }

  initCards() {
    const deck = new Deck(52, {
      type: 'classic',
      color: 'dark',
    });

    // spread cards to game stacks
    this.gameElements.roulette = deck.cards.filter((card: Card) => {
      return card.family = 'clover';
    });

    console.log('this.gameElements', this.gameElements);

  }

  nextTurn() {
    this.turn++;
  }
}
