import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonnaieLibreComponent } from './monnaie-libre.component';

describe('MonnaieLibreComponent', () => {
  let component: MonnaieLibreComponent;
  let fixture: ComponentFixture<MonnaieLibreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonnaieLibreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonnaieLibreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
