import {Component, OnInit} from '@angular/core';
import {GameLibreService} from '../../services/game-libre.service';
import {Player} from '../../models/player';
import {DealerService} from '../../services/dealer.service';

@Component({
  selector: 'app-monnaie-libre',
  templateUrl: './monnaie-libre.component.html',
  styleUrls: ['./monnaie-libre.component.scss']
})
export class MonnaieLibreComponent implements OnInit {

  displayLogs = false;
  public players: Player[];

  constructor(private game: GameLibreService, public dealer: DealerService) {
  }

  ngOnInit() {

    this.players = this.dealer.players;
    this.dealer.startLibre();
  }

  runSimulation() {
    this.dealer.startLibre();
  }

}
