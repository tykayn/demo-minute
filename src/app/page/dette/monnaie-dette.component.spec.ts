import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonnaieDetteComponent } from './monnaie-dette.component';

describe('DetteComponent', () => {
  let component: MonnaieDetteComponent;
  let fixture: ComponentFixture<MonnaieDetteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonnaieDetteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonnaieDetteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
