import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {MonnaieDetteComponent} from './page/dette/monnaie-dette.component';
import {MonnaieLibreComponent} from './page/monnaie-libre/monnaie-libre.component';
import {PlayerComponent} from './ui/player/player.component';
import {DeckComponent} from './ui/deck/deck.component';
import {CardComponent} from './ui/card/card.component';
import {FormsModule} from '@angular/forms';
import {TableScoresComponent} from './ui/table-scores/table-scores.component';

import {SocketIoConfig, SocketIoModule} from 'ngx-socket-io';
import { ChatComponent } from './ui/chat/chat.component';
import { OptionsComponent } from './ui/options/options.component';
import { ChartBatailleComponent } from './ui/chart-bataille/chart-bataille.component';

const config: SocketIoConfig = {url: 'http://localhost:8988', options: {}};


@NgModule({
  declarations: [
    AppComponent,
    MonnaieDetteComponent,
    MonnaieLibreComponent,
    PlayerComponent,
    DeckComponent,
    CardComponent,
    TableScoresComponent,
    ChatComponent,
    OptionsComponent,
    ChartBatailleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    SocketIoModule.forRoot(config)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
